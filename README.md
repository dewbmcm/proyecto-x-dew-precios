![](https://bytebucket.org/dewbmcm/proyecto-x-dew-precios/raw/1b34a3959f28de58f740bc9f5c5aa71ce47dd640/assets/svg/logo.png)
# Bebay

## Project links.
 * [Github Pages](https://manolosocasschmid.github.io/Bebay/#)
 * [Bitbucket](https://bitbucket.org/dewbmcm/proyecto-x-dew-precios).
 * [Trello](https://trello.com/b/UXfcQ8tT/proyecto-x-dew-precios).
 * [Slack](https://dewbmcm.slack.com/messages/C9CGBQHG9/).

### This Web is built to facilitate buy smartphones, Tv and Health, Fitness & Beauty products on the eBay and BestBuy platforms. The users a required to login before they are allowed to buy any of the products. You may login with any account created on gmail, facebook , twitter , github or the own page.

You can see the mockup of the page, in this [link](https://app.moqups.com/manolo.socas3@gmail.com/gVKh7wiAyE/view).
	
## Technology.
* JavaScript.->[code](https://bitbucket.org/dewbmcm/proyecto-x-dew-precios/src/bfb405c941b605cc9858381e61ef6f356525108f/js/?at=master).
* SASS.->[code](https://bitbucket.org/dewbmcm/proyecto-x-dew-precios/src/bfb405c941b605cc9858381e61ef6f356525108f/sass/?at=master).
* CSS.->[code](https://bitbucket.org/dewbmcm/proyecto-x-dew-precios/src/bfb405c941b605cc9858381e61ef6f356525108f/css/?at=master).
* HTML.->[code](https://bitbucket.org/dewbmcm/proyecto-x-dew-precios/src/bfb405c941b605cc9858381e61ef6f356525108f/index.html?at=master&fileviewer=file-view-default).

### We use the next API's in the our project.

***
* [eBay](https://go.developer.ebay.com/quick-start-guide).->[code](https://bitbucket.org/dewbmcm/proyecto-x-dew-precios/src/ca6d37e74b55453fc3b3be9c0fbbba29dc00053b/js/main.js?at=master&fileviewer=file-view-default)
* [Best Buy](https://bestbuyapis.github.io/api-documentation/#get-a-key).->[code](https://bitbucket.org/dewbmcm/proyecto-x-dew-precios/src/ca6d37e74b55453fc3b3be9c0fbbba29dc00053b/js/main.js?at=master&fileviewer=file-view-default)
* [API Graph](https://developers.facebook.com/docs/graph-api/).->[code](https://bitbucket.org/dewbmcm/proyecto-x-dew-precios/src/ca6d37e74b55453fc3b3be9c0fbbba29dc00053b/js/api/apiFacebook.js?at=master&fileviewer=file-view-default)
* [Forex](https://1forge.com/forex-data-api/api-documentation).->[code](https://bitbucket.org/dewbmcm/proyecto-x-dew-precios/src/83d6545d852255fb0d232e4d35c31324556adb46/js/api/apiForex.js?at=cristian&fileviewer=file-view-default)

### [Library](https://bitbucket.org/dewbmcm/proyecto-x-dew-precios/src/ca6d37e74b55453fc3b3be9c0fbbba29dc00053b/js/lib/?at=master).
* [Bootstrap](https://v4-alpha.getbootstrap.com/about/history/).
* [Jquery](https://jquery.com/).
* [Popper](https://popper.js.org/).

To install eslint google:
npm install --save-dev eslint eslint-config-google