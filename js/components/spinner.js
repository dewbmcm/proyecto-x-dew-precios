/**
 *spinner that executs during ajax load.
*/
class Spinner extends React.Component {
    /**
     * @return {ReactComponent}
     */
    render() {
        return (
            <i className="spinner fa fa-spinner fa-spin" />
        );
    }
}
